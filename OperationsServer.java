import OperationsApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

import java.util.Properties;

class OpImpl extends OperationsPOA 
{
  private ORB orb;

  public void setORB(final ORB orb_val) {
      orb = orb_val;
  }

  // implement shutdown() method
  public void shutdown() {
      orb.shutdown(false);
  }

  @Override
  public String suma(final double a, final double b) {
      // TODO Auto-generated method stub
      return String.valueOf(a + b);
  }

  @Override
  public String resta(final double a, final double b) {
      // TODO Auto-generated method stub
      return String.valueOf(a - b);
  }

  @Override
  public String producto(final double a, final double b) {
      // TODO Auto-generated method stub
      return String.valueOf(a * b);
  }

  @Override
  public String division(final double a, final double b) {
      // TODO Auto-generated method stub
      if (b == 0) {
          final String resp = "Division por 0";
          return resp;
      }
      return String.valueOf(a / b);
  }

  @Override
  public String potencia(final double a, final double b) {
      // TODO Auto-generated method stub
      return String.valueOf(Math.pow(a, b));
  }
}

public class OperationsServer {

    public static void main(final String args[]) {
        try {
            // create and initialize the ORB
            final ORB orb = ORB.init(args, null);

            // get reference to rootpoa & activate the POAManager
            final POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootpoa.the_POAManager().activate();

            // create servant and register it with the ORB
            final OpImpl helloImpl = new OpImpl();
            helloImpl.setORB(orb);

            // get object reference from the servant
            final org.omg.CORBA.Object ref = rootpoa.servant_to_reference(helloImpl);
            final Operations href = OperationsHelper.narrow(ref);

            // get the root naming context
            // NameService invokes the name service
            final org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
            // Use NamingContextExt which is part of the Interoperable
            // Naming Service (INS) specification.
            final NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // bind the Object Reference in Naming
            final String name = "Operations";
            final NameComponent path[] = ncRef.to_name(name);
            ncRef.rebind(path, href);

            System.out.println("OperationsServer ready and waiting ...");

            // wait for invocations from clients
            orb.run();
        } catch (final Exception e) 
    {
      System.err.println("ERROR: " + e);
      e.printStackTrace(System.out);
    }

    System.out.println("HelloServer Exiting ...");
  }
}
