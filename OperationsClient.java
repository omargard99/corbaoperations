import OperationsApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.omg.CORBA.*;

public class OperationsClient
{
  static Operations OpImpl;

  public static void main(String args[])
  {
    try
    {
        // create and initialize the ORB
        ORB orb = ORB.init(args, null);

        // get the root naming context
        org.omg.CORBA.Object objRef = 
        orb.resolve_initial_references("NameService");
        // Use NamingContextExt instead of NamingContext. This is 
        // part of the Interoperable naming Service.  
        NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

        // resolve the Object Reference in Naming
        String name = "Operations";
        OpImpl = OperationsHelper.narrow(ncRef.resolve_str(name));

        System.out.println("Obtained a handle on server object: " + OpImpl);
        //System.out.println(OpImpl.sayHello());
        //helloImpl.shutdown();
        String frase = " ";
        InputStreamReader isr1 = new InputStreamReader( System.in );
		BufferedReader inFromUser = new BufferedReader( isr1 );
        while(!frase.equals("bye"))
		{
			System.out.print("Operacion: ");
			frase = inFromUser.readLine();
            String [] arrayMsg = frase.split(" ");
			String ret="";
			Double a1=Double.parseDouble(arrayMsg[0]);
			String sign = arrayMsg[1];
			Double a2=Double.parseDouble(arrayMsg[2]);

			switch(sign) 
			{
				case "+":
				{
					ret=OpImpl.suma(a1,a2);
					break;
				}
				case "-":
				{
					ret=OpImpl.resta(a1,a2);
					break;
				}
				case "*":
				{
					ret=OpImpl.producto(a1,a2);
					break;
				}
				case "/":
				{
					ret=OpImpl.division(a1,a2);
					break;
				}
				case "^":
				{
					ret=OpImpl.potencia(a1,a2);
					break;
				}
			}
			System.out.println("Resultado: "+ret);
		}
		OpImpl.shutdown();

    }catch (Exception e) {
      System.out.println("ERROR : " + e) ;
      e.printStackTrace(System.out);
    }
  }
}
